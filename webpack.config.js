var path = require('path');

const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    entry: "./src/main.jsx",
    output:{
      path: path.resolve(__dirname, './public/build'),    
      publicPath: '/public/build/',
      filename: "bundle.js"    
    },
    // output: {
    //     path: __dirname + "/public/build",
    //     filename: "bundle.js"
    // },
    watch: true,
    watchOptions: {
        aggregateTimeout: 100
    },    
    devServer: {
      historyApiFallback: true,
    },
    module:{
        loaders:[
          {
            test:/\.js$/,
            loader: "babel-loader",
            exclude:[/node_modules/, /public/],
            query:{
              presets:['es2015', 'react']
            }
          },
          {
            test:/\.css$/,
            loader: ExtractTextPlugin.extract({
              fallback: "style-loader",
              use: "css-loader!autoprefixer-loader"
            }),
            exclude:[/node_modules/, /public/]
          },
          {
            test: /\.less$/,
            loader: ExtractTextPlugin.extract({
              fallback: "style-loader",
              use: "css-loader!autoprefixer-loader!less-loader"
            }),
            exclude:[/node_modules/, /public/]
          },
          {
            test:/\.jsx$/,
            loader: "babel-loader",
            exclude:[/node_modules/, /public/],
            query:{
              presets:['es2015', 'react']
            }
          },
          {
            test:/\.json$/,
            loader: "json-loader"
          }
        ]
      },
      plugins: [
        new ExtractTextPlugin("style.css")
      ]
}