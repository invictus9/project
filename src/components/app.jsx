import React from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';

import ControlPanel from './controlPanel.jsx';
import EmployeesList from './employeesList.jsx';


import './app.less';



class App extends React.Component {
    constructor(props) {
        super(props)                
    }
    render() {
        return (
            <div className="app">

                <div className="linkPanel">
                    <NavLink to="/employeesList" className="link">to employees list</NavLink>
                </div>

                <div className="content">
                    <Switch>
                        <Route exact path="/" component={() => <div>choose the action</div>} />
                        <Route exact path="/employeesList" render={() => <div>employeeList</div>} />
                        <Route render={() => <div>error 404 not found</div>} />
                    </Switch>
                </div>
            </div>
        )
    }
}
export default App;

