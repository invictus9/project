import React from 'react';
import './employee.less';
import { NavLink } from 'react-router-dom';

class Employee extends React.Component {
    constructor(props) {
        super(props)

    }

    render() {
        return (
            <div className="employee" >
                <div>{this.props.employee.name}</div>
                <div>{this.props.employee.role}</div>
                <div>{this.props.employee.phone}</div>
                <NavLink to={`/employeePreview/${this.props.employee.id}`}>to profile</NavLink>
            </div>
        )
    }
}

export default Employee;