import React from 'react'
import './EmployeePreview.less'
import { NavLink } from 'react-router'

class EmployeePreview extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const prodId = this.props.match.params.id;
        console.log(prodId)
        return (
            <div className="employeePreview">
                <NavLink to="/">Back to list</NavLink>
                {this.props.name}
                {this.props.phone}
                {this.props.birthday}
                {this.props.role}
                {this.props.isArchive}        
            </div>
        )
    }
}

export default EmployeePreview;

 